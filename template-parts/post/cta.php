<?php
/**
 * Template part for displaying post CTA
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package osinum-diag
 */
$cta = get_field( 'cta', 'option' );
if ( ! $cta ) {
    return;
}
$link = $cta[ 'link' ];
if ( ! $link ) {
    return;
}
?>

<div class="post-cta alignfull">
    <div class="grid-2">
        <?php if ( $cta[ 'title' ] ) : ?>
            <h3><?php echo $cta[ 'title' ]; ?></h3>
        <?php endif; ?>
        <?php
        if ( is_array( $link ) && isset( $link[ 'title' ] ) && ! empty( $link[ 'title' ] ) && isset( $link[ 'url' ] ) && ! empty( $link[ 'url' ] ) ) :
            printf(
                '<div class="wp-block-button"><a href="%1$s" class="wp-block-button__link"%2$s>%3$s</a></div>',
                esc_url( $link[ 'url' ] ),
                isset( $link[ 'target' ] ) && ! empty( $link[ 'target' ] ) ? ' target="_blank"' : '',
                esc_attr( $link[ 'title' ] )
            );
        endif;
        ?>
    </div>
</div>