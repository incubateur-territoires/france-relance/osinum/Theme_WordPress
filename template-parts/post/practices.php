<?php
/**
 * Template part for displaying post practices (terms)
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package osinum-diag
 */

$practices = get_the_terms( get_the_ID(), 'practice' );
if ( ! $practices ) {
    return;
}
?>
<div class="post-practices alignfull">
    <h2><?php _e( 'Pratiques possibles', 'osiunm-diag' ); ?></h2>
    <div class="swiper-element">
        <div class="swiper" data-slides="3.5" data-slides-mobile="1.5">
            <div class="swiper-wrapper">
                <?php foreach ( $practices as $practice ) {
                    _ositer()->get_frontend()->template->render_card( $practice->term_id, 'practice' );
                } ?>
            </div>
        </div>
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
    </div>
</div>