<?php
/**
 * Template part for displaying single post footer
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package osinum-diag
 */
$post_type_obj = get_post_type_object( get_post_type() );
if ( is_wp_error( $post_type_obj ) ) {
    return;
}
if ( ! $post_type_obj->has_archive ) {
    return;
}
?>

<div class="entry-cta wp-block-button">
    <p><?php printf( __( 'More %s are waiting for you!', 'osinum-diag' ), $post_type_obj->labels->name ); ?></p>
    <a href="<?php echo esc_url( get_post_type_archive_link( get_post_type() ) ); ?>" class="wp-block-button__link"><?php _e( 'Go back to search', 'osinum-diag' ); ?></a>
</div>