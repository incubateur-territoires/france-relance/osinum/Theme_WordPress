<?php
/**
 * Template part for displaying post metas
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package osinum-diag
 */

$criterias = get_the_terms( get_the_ID(), 'criteria' );
if ( ! $criterias ) {
    return;
}
$criterias_ids = wp_list_pluck( $criterias, 'term_id' );
$all_criterias = get_terms( [ 'taxonomy' => 'criteria', 'hide_empty' => true, 'fields' => 'ids' ] );
?>

<div class="post-metas">
    <ul class="tool-criterias">
        <?php foreach( $all_criterias as $criteria ) {
            printf(
                '<li class="tool-criteria %1$s"><span class="tool-criteria__icon">%2$s</span>%3$s</li>',
                in_array( $criteria, $criterias_ids ) ? 'tool-criteria--active' : '',
                in_array( $criteria, $criterias_ids ) ? '<i class="icon__ icon-check"></i>' : '',
                get_term( $criteria, 'criteria' )->name
            );
        } ?>
    </ul>
</div>