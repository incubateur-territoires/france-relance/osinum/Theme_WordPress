<?php
/**
 * Template part for displaying post metas
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package osinum-diag
 */

?>

<div class="post-metas">
    <div>
        <?php osinum_posted_on(); ?>
        <?php osinum_posted_by(); ?>
    </div>
</div>