<?php
/**
 * Template part for displaying post reviews
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package osinum-diag
 */
$reviews = get_field( 'reviews' );
if ( ! $reviews ) {
    return;
}
?>

<div class="post-reviews alignfull">
    <h2><?php _e( 'Osinum reviews', 'osinum-diag' ); ?></h2>
    <div class="grid-2">
        <?php if ( $reviews[ 'tips' ] ) : ?>
            <div>
                <h3><i class="icon__ icon-medical-chat" aria-hidden="true"></i><?php _e( 'Tips', 'osinum-diag' ); ?></h3>
                <?php echo $reviews[ 'tips' ]; ?>
            </div>
        <?php endif; ?>
        <?php if ( $reviews[ 'warning' ] ) : ?>
            <div>
                <h3><i class="icon__ icon-Thunder" aria-hidden="true"></i><?php _e( 'Warning', 'osinum-diag' ); ?></h3>
                <?php echo $reviews[ 'warning' ]; ?>
            </div>
        <?php endif; ?>
    </div>
</div>