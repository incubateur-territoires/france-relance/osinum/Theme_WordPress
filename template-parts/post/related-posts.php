<?php
/**
 * Template part for displaying related posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package osinum-diag
 */

$categories = get_the_category( get_the_ID() );
if ( ! $categories ) {
    return;
}

$related_posts = new WP_Query( [
    'post_type' => 'post',
    'post_status'   => 'publish',
    'cat'           => wp_list_pluck( $categories, 'ID' ),
    'post__not_in'  => [ get_the_ID() ]
] );
if ( ! $related_posts->have_posts() ) {
    return;
}
?>

<div class="related-posts">
    <h2><?php _e( 'Related posts', 'osinum-diag' ); ?></h2>
    <div class="swiper-element">
        <div class="swiper" data-slides="2" data-slides-mobile="1.1" data-space="24">
            <div class="swiper-wrapper">
                <?php
                while ( $related_posts->have_posts() ) :
                    $related_posts->the_post();
                
                    get_template_part( 'template-parts/content', get_post_type(), [ 'class' => 'swiper-slide', 'heading' => 'h3' ] );
                
                endwhile; // End of the loop.
                wp_reset_postdata();
                ?>
            </div>
        </div>
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
    </div>
</div>