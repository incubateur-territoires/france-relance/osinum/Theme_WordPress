<?php
/**
 * Template part for displaying related posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package osinum-diag
 */


$args = [
    'post_type' => 'resource',
    'post_status'   => 'publish',
    'post__not_in'  => [ get_the_ID() ]
];

$topics = get_the_terms( get_the_ID(), 'topic' );
$difficulty_groups = get_the_terms( get_the_ID(), 'difficulty_group' );
$tax_query = [];
if ( $topics ) {
    $tax_query[] = [
        'taxonomy'  => 'topic',
        'field'     => 'term_id',
        'terms'     => wp_list_pluck( $topics, 'term_id' )
    ];
}
if ( $difficulty_groups ) {
    $tax_query[] = [
        'taxonomy'  => 'difficulty_group',
        'field'     => 'term_id',
        'terms'     => wp_list_pluck( $difficulty_groups, 'term_id' )
    ];
}
if( ! empty( $tax_query ) ) {
    $tax_query[ 'relation' ] = 'OR';
    $args[ 'tax_query' ][] = $tax_query;
}

$related_posts = new WP_Query( $args );

if ( ! $related_posts->have_posts() ) {
    return;
}
?>

<div class="related-posts">
    <h2><?php _e( 'Related resources', 'osinum-diag' ); ?></h2>
    <div class="swiper-element">
        <div class="swiper" data-slides="2.5" data-slides-mobile="1.1" data-space="24">
            <div class="swiper-wrapper">
                <?php
                while ( $related_posts->have_posts() ) :
                    $related_posts->the_post();
                    _ositer()->get_frontend()->template->render_card( get_the_ID(), 'resource', 'post-card__cover swiper-slide' );
                
                endwhile; // End of the loop.
                wp_reset_postdata();
                ?>
            </div>
        </div>
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
    </div>
    <div class="wp-block-button">
        <a href="<?php echo esc_url( get_post_type_archive_link( 'resource' ) ); ?>" class="wp-block-button__link"><?php _e( 'See more resources', 'osinum-diag' ); ?></a>
    </div>
</div>