<?php
/**
 * Template part for displaying post testimonials
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package osinum-diag
 */
$testimonials = get_field( 'testimonials' );
if ( ! $testimonials ) {
    return;
}
?>

<div class="post-testimonials alignfull">
    <h2><?php _e( 'Testimonials', 'osinum-diag' ); ?></h2>
    <div class="swiper-element">
        <div class="swiper" data-slides="3.4" data-slides-mobile="1.4" data-space="24">
            <div class="swiper-wrapper">
                <?php
                if( have_rows('testimonials') ):
                    while( have_rows('testimonials') ) : the_row();
                    $name = get_sub_field( 'name' );
                    $text = get_sub_field( 'text' );
                    if ( ! $name || ! $text ) {
                        continue;
                    }
                    $image = get_sub_field( 'image' );                    
                    $position = get_sub_field( 'position' );
                    $date = get_sub_field( 'date' );
                    ?>
                    <div class="swiper-slide">
                        <div class="testimonial">
                            <?php if ( $image ) : ?>
                                <?php echo wp_get_attachment_image( $image, 'square' ); ?>
                            <?php endif; ?>
                            <p class="testimonial__name"><?php echo $name; ?></p>
                            <?php if ( $position ) : ?>
                                <p class="testimonial__position"><?php echo $position; ?></p>
                            <?php endif; ?>
                            <div class="testimonial__text"><?php echo $text; ?></div>
                            <?php if ( $date ) : ?>
                                <p class="testimonial__date"><?php echo $date; ?></p>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php
                    endwhile;
                endif; ?>
            </div>
        </div>
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
    </div>
</div>