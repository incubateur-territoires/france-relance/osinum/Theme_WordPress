<?php
/**
 * Template part for displaying single gallery
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package osinum-diag
 */
if ( ! get_field( 'gallery' ) ) {
    return;
}
?>

<div class="post-gallery alignfull">
    <div class="swiper-element">
        <div class="swiper" data-slides="2" data-slides-mobile="1">
            <div class="swiper-wrapper">
                <?php
                $index = 0;
                foreach( get_field( 'gallery' ) as $image ) :
                    $image_data = wp_get_attachment_image_src( $image, 'large' );
                    ?>
                    <div class="swiper-slide" data-slide-index="<?php esc_attr_e( $index ); ?>">
                        <a href="<?php echo esc_url( $image_data[0] ); ?>" data-gallery-open="gallery-modal">
                            <?php echo wp_get_attachment_image( $image, 'medium-large' ); ?>
                        </a>
                    </div>
                <?php $index++; endforeach; ?>
            </div>
        </div>
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
    </div>
</div>

<div id="gallery-modal" class="gallery-modal micromodal-slide" aria-hidden="true">
  <div tabindex="-1" class="modal__overlay" data-gallery-close>
    <div class="modal__container">
        <button class="modal__close" aria-label="<?php _e( 'Close', 'osinum-diag' ); ?>" data-gallery-close>
			<svg aria-hidden="true" class="icon fermer"><use xlink:href="#fermer"></use></svg>
        </button>
        <div class="swiper-element">
            <div class="swiper" data-slides="1" data-slides-mobile="1">
                <div class="swiper-wrapper">
                    <?php
                    $index = 0;
                    foreach( get_field( 'gallery' ) as $image ) :
                    ?>
                        <div class="swiper-slide" data-slide-index="<?php esc_attr_e( $index ); ?>">
                           <?php echo wp_get_attachment_image( $image, 'medium-large' ); ?>
                        </div>
                    <?php $index++; endforeach; ?>
                </div>
            </div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
        </div>
    </div>
  </div>
</div>