<?php

defined( 'ABSPATH' ) || die();

/** @var OsinumTerritoire\Models\Practice $practice */
$practice = isset( $args ) ? $args['practice'] : $practice;
$link = get_post_type_archive_link( 'tool' );
if ( function_exists( 'wp_grid_builder' ) ) {
	$link .= '?_pratiques=' . $practice->get_slug();
}
?>

<article class="post-card practice swiper-slide">
	<div class="post-card__inner">
		<?php ositer()->icon( 'ecran' ); ?>
		<h3 class="entry-title"><a href="<?php echo esc_url( $link ); ?>"><?php echo $practice->get_name(); ?></a></h3>
		<p class="label"><?php _e( 'pratique', 'ositer' ); ?></p>
	</div>
</article>
