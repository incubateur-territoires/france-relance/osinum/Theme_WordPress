<?php

defined( 'ABSPATH' ) || die();

/** @var OsinumTerritoire\Models\Resource $resource */
$resource = isset( $args ) ? $args['resource'] : $resource;
$class     = isset( $args['class'] ) ? $args['class'] : '';
$heading = isset( $args[ 'heading' ] ) && ! empty( $args[ 'heading' ] ) ? $args[ 'heading' ] : 'h3';
?>

<article class="post-card resource <?php echo $class; ?>">
	<div class="post-card__inner">
		<?php echo osinum_post_format_icon( get_the_ID() ); ?>
		<?php echo $resource->get_thumbnail_img( 'entry-thumbnail' ) ?>
		<<?php echo $heading; ?> class="entry-title"><a href="<?php echo $resource->get_permalink(); ?>" rel="bookmark"><?php echo $resource->get_name(); ?></a></<?php echo $heading; ?>>
		<p class="excerpt"><?php echo $resource->get_excerpt(); ?></p>
		<ol class="practices-list">
			<?php foreach ( $resource->get_topics() as $topic ) {
				printf(
					'<li class="topic has-icon"><span class="button theme-%2$s"><svg aria-hidden="true" class="icon %3$s"><use xlink:href="#%3$s"></use></svg></span> %1$s</li>',
					$topic->get_name(),
					$topic->get_meta( 'color' ),
					$topic->get_meta( 'icon' )
				);
			} ?>
			<?php foreach ( $resource->get_difficulty_groups() as $difficulty_group ) {
				printf(
					'<li class="difficulty-group has-icon"><span class="button theme-%2$s-dark"><svg aria-hidden="true" class="icon %3$s"><use xlink:href="#%3$s"></use></svg></span> %1$s</li>',
					$difficulty_group->get_name(),
					$difficulty_group->get_meta( 'color' ),
					$difficulty_group->get_meta( 'icon' )
				);
			} ?>
		</ol>
		<?php get_template_part( 'template-parts/post/metas', get_post_type() ); ?>
	</div>
</article>
