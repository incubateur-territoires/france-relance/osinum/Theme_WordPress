<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package osinum-idag
 */

$class = isset( $args[ 'class' ] ) && ! empty( $args[ 'class' ] ) ? $args[ 'class' ] : '';
$class .= ' post-card post-card__cols post-card--full';
$heading = isset( $args[ 'heading' ] ) && ! empty( $args[ 'heading' ] ) ? $args[ 'heading' ] : 'h2';
?>

<article id="<?php esc_attr_e( $post->post_name ); ?>" <?php post_class( $class ); ?>>
    <div class="post-card__inner">
		<?php the_title( '<' . $heading . ' class="entry-title">', '</' . $heading . '>' ); ?>
		<div class="description">
			<?php the_content(); ?>
		</div>
	</div>
	<div class="post-card__inner">
		<?php the_post_thumbnail( 'thumbnail' ); ?>
        <?php osinum_diag_member_social(); ?>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->
