<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package osinum-idag
 */

?>

<section class="no-results not-found entry-content">
	<header class="page-header">
		<h1 class="page-title"><?php esc_html_e( 'Nothing Found', 'osinum-idag' ); ?></h1>
	</header><!-- .page-header -->

	<div class="page-content">
		<?php
		if ( is_home() && current_user_can( 'publish_posts' ) ) :

			printf(
				'<p>' . wp_kses(
					/* translators: 1: link to WP admin new post page. */
					__( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'osinum-idag' ),
					array(
						'a' => array(
							'href' => array(),
						),
					)
				) . '</p>',
				esc_url( admin_url( 'post-new.php' ) )
			);

		elseif ( is_search() ) :
			?>

			<p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'osinum-idag' ); ?></p>
			<?php
			get_search_form();

		else :
			?>

			<p><?php printf( __( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Please <a href="%s">contact us</a>.', 'osinum-idag' ), esc_url( get_home_url() . '/contact' ) ); ?></p>
			<div class="wp-block-button">
				<a href="<?php echo esc_url( get_home_url() ); ?>" class="wp-block-button__link"><?php _e( 'Go to the homepage', 'osinum-diag' ); ?></a>
			</div>
			<?php
		endif;
		?>
	</div><!-- .page-content -->
</section><!-- .no-results -->
