<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package osinum-idag
 */
$class = isset( $args[ 'class' ] ) && ! empty( $args[ 'class' ] ) ? $args[ 'class' ] : '';
$class .= ' post-card';
$heading = isset( $args[ 'heading' ] ) && ! empty( $args[ 'heading' ] ) ? $args[ 'heading' ] : 'h2';
$tool = new \OsinumTerritoire\Models\Tool( get_the_ID() );
$practices = array_map( fn( $practice_id ) => ositer()->get_practice( $practice_id ), $tool->get_practices( true ) );
?>

<article class="post-card tool swiper-slide <?php echo $class; ?>">
	<?php if ( str_contains( $class, 'post-card--large' ) && get_field( 'gallery', $tool->get_id() ) ) : ?>
		<div class="post-card__inner post-card__thumb">
			<?php echo wp_get_attachment_image( get_field( 'gallery', $tool->get_id() )[0], 'large' ); ?>
		</div>
	<?php endif; ?>
	<div class="post-card__inner">
		<div class="post-card__inner_left">
			<?php echo $tool->get_thumbnail_img( 'square', [ 'class' => 'attachment-square size-square img-rounded' ] ) ?>
			<<?php echo $heading; ?> class="entry-title"><a href="<?php echo $tool->get_permalink(); ?>" rel="bookmark"><?php echo $tool->get_name(); ?></a></<?php echo $heading; ?>>
		</div>
		<p class="excerpt"><?php echo $tool->get_excerpt(); ?></p>
		<div class="description"><?php echo wp_trim_words( wpautop( $tool->get_description() ), 20 ); ?></div>
		<ol class="practices-list">
			<?php foreach ( $practices as $practice ) {
				printf( '<li>%2$s</li>', $practice->get_permalink(), $practice->get_name() );
			} ?>
		</ol>
	</div>
</article>
