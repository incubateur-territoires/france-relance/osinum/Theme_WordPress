<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package osinum-diag
 */
$tool = new \OsinumTerritoire\Models\Tool( get_the_ID() );
$archive_link = get_post_type_archive_link( 'tool' ) . '?';
$desc = $tool->get_description( false );
if ( str_contains( $desc, '<!--more-->' ) ) {
    $strs = explode( '<!--more-->', $desc, 2 );
    if ( isset( $strs[0] ) && ! empty( $strs[0] ) && isset( $strs[1] ) && ! empty( $strs[1] ) ) {
        $id = uniqid( 'read-more-' );
        $desc = sprintf(
            '%1$s <button class="single-toggle" aria-expanded="false" aria-controls="%4$s">%2$s</button><div class="single__read-more single__read-more--hidden" id="%4$s">%3$s</div>',
            wpautop( $strs[0] ),
            __( 'read more', 'osinum' ),
            wpautop( $strs[1] ),
            $id
        );
    }
} else {
    $desc = wpautop( $desc );
}
?>
<section class="widget-panel home">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

        <header class="entry-header alignwide">
            <?php if ( function_exists( 'seopress_display_breadcrumbs' ) ) : ?>
                <div class="breadcrumbs">
                    <?php seopress_display_breadcrumbs(); ?>		
                </div>
            <?php endif; ?>
            <div class="entry-excerpt">
                <?php echo $tool->get_thumbnail_img( 'square', [ 'class' => 'attachment-square size-square img-rounded' ] ) ?>
                <div>
                    <h1 class="entry-title"><?php echo $tool->get_name(); ?></h1>
                    <p class="excerpt"><?php echo $tool->get_excerpt(); ?></p>
                </div>
            </div>
            <?php get_template_part( 'template-parts/post/metas', get_post_type() ); ?>
        </header><!-- .entry-header -->

        <div class="entry-content alignwide">
            <?php get_template_part( 'template-parts/post/gallery' ); ?>

            <div class="entry-content__inner alignfull grid-2-1">
                <div><?php echo apply_filters('the_content', get_the_content()); ?></div>
                <div class="entry-content__buttons">
                    <?php
                    if( have_rows('links') ):
                        while( have_rows('links') ) : the_row();
                            $link = get_sub_field('link');
                            if ( is_array( $link ) && isset( $link[ 'title' ] ) && ! empty( $link[ 'title' ] ) && isset( $link[ 'url' ] ) && ! empty( $link[ 'url' ] ) ) :
                                printf(
                                    '<div class="wp-block-button"><a href="%1$s" class="wp-block-button__link"%2$s>%3$s</a></div>',
                                    esc_url( $link[ 'url' ] ),
                                    isset( $link[ 'target' ] ) && ! empty( $link[ 'target' ] ) ? ' target="_blank"' : '',
                                    esc_attr( $link[ 'title' ] )
                                );
                            endif;
                        endwhile;

                    endif; ?>
                </div>
            </div>

            <?php get_template_part( 'template-parts/post/practices' ); ?>

            <?php get_template_part( 'template-parts/post/reviews' ); ?>

            <?php get_template_part( 'template-parts/post/testimonials' ); ?>

            <?php get_template_part( 'template-parts/post/cta' ); ?>
        </div><!-- .entry-content -->

        <footer class="entry-footer alignwide">
            <?php get_template_part( 'template-parts/post/related-resources' ); ?>
            <?php get_template_part( 'template-parts/post/cta-archive' ); ?>
        </footer><!-- .entry-footer -->

    </article><!-- #post-<?php the_ID(); ?> -->
</section>

<?php include_once _ositer()->get_frontend()->template->get_template_file( 'steps/partials/tool-sidebar' ); ?>