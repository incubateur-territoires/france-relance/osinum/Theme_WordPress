<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package osinum-diag
 */
$resource = new \OsinumTerritoire\Models\Resource( get_the_ID() );
$archive_link = get_post_type_archive_link( 'resource' ) . '?';
?>
<section class="widget-panel home">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

        <header class="entry-header alignwide">
            <?php if ( function_exists( 'seopress_display_breadcrumbs' ) ) : ?>
                <div class="breadcrumbs">
                    <?php seopress_display_breadcrumbs(); ?>		
                </div>
            <?php endif; ?>
            <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
            <ol class="practices-list practices-list--big">
                <?php if ( $resource->get_topics() ) {
                    foreach ( $resource->get_topics() as $topic ) {
                        printf(
                            '<li class="topic has-icon"><a href="%4$s"><span class="button theme-%2$s">%3$s</span> %1$s</a></li>',
                            $topic->get_name(),
                            $topic->get_meta( 'color' ),
                            ositer()->icon( $topic->get_meta( 'icon' ), false ),
                            esc_url( $archive_link . '&_thematiques=' . $topic->get_slug() )
                        );
                    }
                } ?>
                <?php if ( $resource->get_difficulty_groups() ) {
                    foreach ( $resource->get_difficulty_groups() as $difficulty_group ) {
                        printf(
                            '<li class="difficulty-group has-icon"><a href="%4$s"><span class="button theme-%2$s-dark">%3$s</span> %1$s</a></li>',
                            $difficulty_group->get_name(),
                            $difficulty_group->get_meta( 'color' ),
                            ositer()->icon( $difficulty_group->get_meta( 'icon' ), false ),
                            esc_url( $archive_link . '&_thematiques=' . $difficulty_group->get_slug() )
                        );
                    }
                } ?>
            </ol>
            <?php get_template_part( 'template-parts/post/metas', get_post_type() ); ?>
        </header><!-- .entry-header -->

        <div class="entry-content alignwide">
            <?php the_content(); ?>
        </div><!-- .entry-content -->

        <footer class="entry-footer alignwide">
            <?php get_template_part( 'template-parts/post/related-resources' ); ?>
            <?php get_template_part( 'template-parts/post/cta-archive' ); ?>
        </footer><!-- .entry-footer -->

    </article><!-- #post-<?php the_ID(); ?> -->
</section>

<?php include_once _ositer()->get_frontend()->template->get_template_file( 'steps/partials/resource-sidebar' ); ?>