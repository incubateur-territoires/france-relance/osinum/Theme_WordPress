<?php

defined( 'ABSPATH' ) || die();

/** @var OsinumTerritoire\Models\Diagnostic $diagnostic */

$description = get_the_archive_description();
$filtered = false;
if ( function_exists( 'wpgb_has_selected_facets' ) && wpgb_has_selected_facets() ) {
    $filtered = true;
}
?>

<section class="widget-panel home">

    <header class="archive-header">
        <?php the_archive_title( '<h1 class="archive-title">', '</h1>' ); ?>
        <?php if ( $description ) : ?>
            <div class="archive-description"><?php echo wp_kses_post( wpautop( $description ) ); ?></div>
        <?php endif; ?>
    </header><!-- .page-header -->

    <?php if ( function_exists( 'wpgb_render_facet' ) ) : ?>
        <div class="wpgb-filters">
            <h2 class="wpgb-filters__title"><?php _e( 'Search resources', 'osinum-diag' ); ?></h2>

			<div class="wpgb-filters_line_search">
				<div class="wpgb-filters__search">
					<?php
					wpgb_render_facet( [ // search
						'id'    => 1,
						'grid'  => 'wpgb-content'
					] );
					?>
				</div>
			</div>
            
            <button class="wpgb-filters__button" data-wpgb-open="wpgb-filters-selects"><?php _e( 'Filter resources', 'osinum-diag' ); ?></button>
            
            <div class="wpgb-filters__selection">
                <?php
                wpgb_render_facet( [ // selection
                    'id'    => 3,
                    'grid'  => 'wpgb-content'
                ] );
                ?>
            </div>
            <div class="wpgb-filters__selects">
                <h3 class="wpgb-filters__subtitle"><?php _e( 'Filter by:', 'osinum-diag' ); ?></h3>
                <?php
                wpgb_render_facet( [ // outils
                    'id'    => 4,
                    'grid'  => 'wpgb-content',
                    'class' => 'wpgb-facet-color--blue-1-c'
                ] );
                wpgb_render_facet( [ // thématiques
                    'id'    => 8,
                    'grid'  => 'wpgb-content'
                ] );
                ?>
            </div>
            <div class="wpgb-filters__selects micromodal-slide" id="wpgb-filters-selects">
                <div tabindex="-1" class="modal__overlay" data-wpgb-close>
                    <div class="modal__container">
                        <button class="wpgb-filters__button-close" data-wpgb-close>
                            <i class="icon__ icon-left-arrow" aria-hidden="true"></i>
                            <?php _e( 'go back', 'osinum-diag' ); ?>
                        </button>
                        <?php
                        wpgb_render_facet( [ // selection
                            'id'    => 3,
                            'grid'  => 'wpgb-content'
                        ] );
                        ?>
                        <?php
                        wpgb_render_facet( [ // outils
                            'id'    => 4,
                            'grid'  => 'wpgb-content',
                            'class' => 'wpgb-facet-color--blue-1-c'
                        ] );
                        wpgb_render_facet( [ // thématiques
                            'id'    => 8,
                            'grid'  => 'wpgb-content'
                        ] );
                        ?>
                        <button class="wpgb-filters__button-apply" data-wpgb-close><?php _e( 'Ok', 'osinum-diag' ); ?></button>
                        
                    </div>
                </div>
            </div>
            <div class="wpgb-filters__footer">
                <?php
                wpgb_render_facet( [ // results count
                    'id'    => 5,
                    'grid'  => 'wpgb-content'
                ] );
                ?>
                <button class="wpgb-see-results" aria-controls="wpgb-results"><?php _e( 'Voir tous les résultats', 'osinum' ) ?></button>
                <?php
                wpgb_render_facet( [ // reset
                    'id'    => 6,
                    'grid'  => 'wpgb-content'
                ] );
                ?>
            </div>
        </div>
    <?php endif; ?>

    <div id="wpgb-results" class="archive__intro wpgb-active--hidden <?php echo $filtered ? 'wpgb-hidden' : ''; ?>">
        <?php
        $populars = ositer()->get_setting( 'resource_populars' ) ? ositer()->get_setting( 'resource_populars' ) : '';
        if ( $populars ) {
            $populars_posts = new WP_Query( [
                'post_type'     => 'resource',
                'post_status'   => 'any',
                'post__in'      => $populars,
                'posts_per_page'=> count( $populars ),
                'order_by'      => 'post__in'
            ] );
            ?>
            <div class="archive__popular">
                <h2><?php _e( 'Popular resources', 'osinum-diag' ); ?></h2>
                <div class="swiper-element">
                    <div class="swiper" data-slides="3" data-spaces="24" data-slides-mobile="1.1">
                        <div class="swiper-wrapper">
                            <?php while ( $populars_posts->have_posts() ) :
                                $populars_posts->the_post();
                                _ositer()->get_frontend()->template->render_card( get_the_ID(), 'resource', 'post-card__cover swiper-slide' );
                            endwhile;
                            wp_reset_postdata();
                            ?>
                        </div>
                    </div>
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                </div>
            </div>
            <?php
        }
        ?>
    </div>

    <div class="archive__facets wpgb-active--hidden <?php echo $filtered ? 'wpgb-hidden' : ''; ?>">
        <h2><?php _e( 'Search by', 'osinum-diag' ); ?></h2>
        <?php if ( function_exists( 'wpgb_render_facet' ) ) : ?>
            <?php
            wpgb_render_facet( [ // thematiques buttons
                'id'    => 10,
                'grid'  => 'wpgb-content'
            ] );
            wpgb_render_facet( [ // outils buttons
                'id'    => 11,
                'grid'  => 'wpgb-content'
            ] );
            ?>
        <?php endif; ?>
    </div>

    <div class="wpgb-active--show <?php echo $filtered ? '' : 'wpgb-hidden'; ?>">
        <div class="wpgb-archive grid-3">
            <?php while ( have_posts() ) :
                the_post();
                _ositer()->get_frontend()->template->render_card( get_the_ID(), 'resource', 'post-card__cover' );
            endwhile; ?>
        </div>
    </div>

    <?php if ( function_exists( 'wpgb_render_facet' ) ) : ?>
        <?php
        wpgb_render_facet( [ // pagination
            'id'    => 9,
            'grid'  => 'wpgb-content',
            'class' => $filtered ? 'wpgb-active--show' : 'wpgb-active--show wpgb-hidden'
        ] );
        ?>
    <?php endif; ?>

    <aside class="archive-sidebar">
        <div class="resources-sidebar">
            <?php block_template_part( 'sidebar-resources' ); ?>
        </div>
        <div class="resources-sidebar-footer">
            <?php block_template_part( 'footer-resources' ); ?>
        </div>
    </aside>

</section>