<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package osinum-diag
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header">
        <?php if ( function_exists( 'seopress_display_breadcrumbs' ) ) : ?>
            <div class="breadcrumbs">
                <?php seopress_display_breadcrumbs(); ?>		
            </div>
		<?php endif; ?>
        <?php the_post_thumbnail( 'medium-large' ); ?>
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
        <?php get_template_part( 'template-parts/post/metas', get_post_type() ); ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php the_content(); ?>
	</div><!-- .entry-content -->

    <?php if ( 'post' === get_post_type() ) : ?>
        <footer class="entry-footer">
            <?php get_template_part( 'template-parts/post/related-posts' ); ?>
        </footer><!-- .entry-footer -->
    <?php endif; ?>

</article><!-- #post-<?php the_ID(); ?> -->
