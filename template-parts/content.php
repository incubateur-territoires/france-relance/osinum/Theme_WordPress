<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package osinum-idag
 */

$class = isset( $args[ 'class' ] ) && ! empty( $args[ 'class' ] ) ? $args[ 'class' ] : '';
$class .= ' post-card post-card__cover';
$heading = isset( $args[ 'heading' ] ) && ! empty( $args[ 'heading' ] ) ? $args[ 'heading' ] : 'h2';
?>

<article id="post-<?php the_ID(); ?>" <?php post_class( $class ); ?>>
	<div class="post-card__inner">
		<i class="icon__ icon-calender"></i>
		<?php the_post_thumbnail( 'entry-thumbnail' ); ?>
		<?php the_title( '<' . $heading . ' class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></' . $heading . '>' ); ?>
		<div class="description">
			<?php the_excerpt(); ?>
		</div>
		<?php get_template_part( 'template-parts/post/metas', get_post_type() ); ?>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->
