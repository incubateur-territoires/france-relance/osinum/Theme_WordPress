<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package osinum-idag
 */

$class = isset( $args[ 'class' ] ) && ! empty( $args[ 'class' ] ) ? $args[ 'class' ] : '';
$class .= ' post-card post-card__cover';
$heading = isset( $args[ 'heading' ] ) && ! empty( $args[ 'heading' ] ) ? $args[ 'heading' ] : 'h2';
global $post;
?>

<article id="post-<?php the_ID(); ?>" <?php post_class( $class ); ?>>
	<div class="post-card__inner">
		<?php the_post_thumbnail( 'entry-thumbnail' ); ?>
		<<?php echo $heading; ?> class="entry-title"><a href="<?php echo esc_url( get_post_type_archive_link( get_post_type( $post ) ) ) . '#' . $post->post_name; ?>"><?php the_title(); ?></a></<?php echo $heading; ?>>
		<div class="description">
			<?php the_excerpt(); ?>
		</div>
		<?php get_template_part( 'template-parts/post/metas', get_post_type() ); ?>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->
