
/**
 * File navigation.js.
 *
 * Handles toggling the navigation menu for small screens and enables TAB key
 * navigation support for dropdown menus.
 */
 ( function() {
	const header = document.getElementById( 'masthead' );
	if ( ! header ) {
		return;
	}

	const siteNavigation = document.getElementById( 'site-navigation' );

	// Return early if the navigation doesn't exist.
	if ( ! siteNavigation ) {
		return;
	}

	const button = header.getElementsByClassName( 'menu-toggle' )[ 0 ];

	// Return early if the button doesn't exist.
	if ( 'undefined' === typeof button ) {
		return;
	}

	const menu = siteNavigation.getElementsByTagName( 'ul' )[ 0 ];

	// Hide menu toggle button if menu is empty and return early.
	if ( 'undefined' === typeof menu ) {
		button.style.display = 'none';
		return;
	}

	if ( ! menu.classList.contains( 'nav-menu' ) ) {
		menu.classList.add( 'nav-menu' );
	}
	// Toggle the .toggled class and the aria-expanded value each time the button is clicked.
	button.addEventListener( 'click', function() {
		siteNavigation.classList.toggle( 'toggled' );

		if ( button.getAttribute( 'aria-expanded' ) === 'true' ) {
			button.setAttribute( 'aria-expanded', 'false' );
		} else {
			button.setAttribute( 'aria-expanded', 'true' );
		}
	} );

	// Remove the .toggled class and set aria-expanded to false when the user clicks outside the navigation.
	document.addEventListener( 'click', function( event ) {
		const isClickInside = header.contains( event.target );

		if ( ! isClickInside ) {
			siteNavigation.classList.remove( 'toggled' );
			button.setAttribute( 'aria-expanded', 'false' );
		}
	} );

	// Get all the link elements within the menu.
	const links = menu.getElementsByTagName( 'a' );

	// Get all the link elements with children within the menu.
	const linksWithChildren = menu.querySelectorAll( '.menu-item-has-children > a, .page_item_has_children > a' );

	// Toggle focus each time a menu link is focused or blurred.
	for ( const link of links ) {
		link.addEventListener( 'focus', toggleFocus, true );
		link.addEventListener( 'blur', toggleFocus, true );
	}

	// Toggle focus each time a menu link with children receive a touch event.
	for ( const link of linksWithChildren ) {
		link.addEventListener( 'touchstart', toggleFocus, false );
	}

	/**
	 * Sets or removes .focus class on an element.
	 */
	function toggleFocus() {
		if ( event.type === 'focus' || event.type === 'blur' ) {
			let self = this;
			// Move up through the ancestors of the current link until we hit .nav-menu.
			while ( ! self.classList.contains( 'nav-menu' ) ) {
				// On li elements toggle the class .focus.
				if ( 'li' === self.tagName.toLowerCase() ) {
					self.classList.toggle( 'focus' );
				}
				self = self.parentNode;
			}
		}

		if ( event.type === 'touchstart' ) {
			const menuItem = this.parentNode;
			event.preventDefault();
			for ( const link of menuItem.parentNode.children ) {
				if ( menuItem !== link ) {
					link.classList.remove( 'focus' );
				}
			}
			menuItem.classList.toggle( 'focus' );
		}
	}
}() );

MicroModal.init({
    onShow: function( modal, body, target ) {
        target.preventDefault();
        const swiper = modal.querySelector('.swiper').swiper;
        const slide = target.target.closest( '.swiper-slide' );
        if ( slide && swiper ) {
            const currentIndex = slide.getAttribute( 'data-slide-index' );
            swiper.slideTo( currentIndex );
        }
    },
    openTrigger: 'data-gallery-open',
    closeTrigger: 'data-gallery-close',
    openClass: 'is-open'
});

window.addEventListener("resize", initWPGBModal);
window.addEventListener("load", initWPGBModal);
function initWPGBModal() {
    const windowWidth = window.innerWidth;
    if ( windowWidth <= 781 ) {
        MicroModal.init({
            onShow: function( modal, body, target ) {
            },
            openTrigger: 'data-wpgb-open',
            closeTrigger: 'data-wpgb-close',
            openClass: 'is-open'
        });
    }
}
const singleReadMore = document.querySelector( '.single-toggle' );
if ( singleReadMore ) {
    singleReadMore.addEventListener( 'click', function( e ) {
        e.preventDefault();
        const content = document.getElementById( e.target.getAttribute( 'aria-controls' ) );
        if ( content ) {
            content.classList.remove( 'single__read-more--hidden' );
            console.log(e)
            e.target.style.display = 'none';
        }
    } );
}
var stickyEl = new Sticky('.wpgb-filters__button', { stickyClass: 'wpgb-filters__button--sticky', marginTop: 10 });
const swiperWrappers = document.querySelectorAll( '.swiper-element' );
[...swiperWrappers].forEach( swiperWrapper => {
    let swiperElem = swiperWrapper.querySelector( '.swiper' );
    var swiperPosts = new Swiper( swiperElem, {
        observer: true,
        observeParents: true,
        speed: 400,
        spaceBetween: 15,
        slidesPerView: swiperElem.getAttribute( 'data-slides-mobile' ) ? swiperElem.getAttribute( 'data-slides-mobile' ) : 1,
        pagination: {
            el: swiperWrapper.querySelector( '.swiper-pagination' ),
            type: 'bullets',
            clickable: true
        },
        navigation: {
            nextEl: swiperWrapper.querySelector( '.swiper-button-next' ),
            prevEl: swiperWrapper.querySelector( '.swiper-button-prev' ),
            clickable: true
        },
        breakpoints: {
            1024: {
                slidesPerView: swiperElem.getAttribute( 'data-slides' ) ? swiperElem.getAttribute( 'data-slides' ) : 3,
                spaceBetween: parseFloat( swiperElem.getAttribute( 'data-space' ) ) ? parseFloat( swiperElem.getAttribute( 'data-space' ) ) : 15
            }
        }
    } );		
} );