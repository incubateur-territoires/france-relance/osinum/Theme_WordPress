const swiperWrappers = document.querySelectorAll( '.swiper-element' );
[...swiperWrappers].forEach( swiperWrapper => {
    let swiperElem = swiperWrapper.querySelector( '.swiper' );
    var swiperPosts = new Swiper( swiperElem, {
        observer: true,
        observeParents: true,
        speed: 400,
        spaceBetween: 15,
        slidesPerView: swiperElem.getAttribute( 'data-slides-mobile' ) ? swiperElem.getAttribute( 'data-slides-mobile' ) : 1,
        pagination: {
            el: swiperWrapper.querySelector( '.swiper-pagination' ),
            type: 'bullets',
            clickable: true
        },
        navigation: {
            nextEl: swiperWrapper.querySelector( '.swiper-button-next' ),
            prevEl: swiperWrapper.querySelector( '.swiper-button-prev' ),
            clickable: true
        },
        breakpoints: {
            1024: {
                slidesPerView: swiperElem.getAttribute( 'data-slides' ) ? swiperElem.getAttribute( 'data-slides' ) : 3,
                spaceBetween: parseFloat( swiperElem.getAttribute( 'data-space' ) ) ? parseFloat( swiperElem.getAttribute( 'data-space' ) ) : 15
            }
        }
    } );		
} );