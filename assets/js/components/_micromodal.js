MicroModal.init({
    onShow: function( modal, body, target ) {
        target.preventDefault();
        const swiper = modal.querySelector('.swiper').swiper;
        const slide = target.target.closest( '.swiper-slide' );
        if ( slide && swiper ) {
            const currentIndex = slide.getAttribute( 'data-slide-index' );
            swiper.slideTo( currentIndex );
        }
    },
    openTrigger: 'data-gallery-open',
    closeTrigger: 'data-gallery-close',
    openClass: 'is-open'
});

window.addEventListener("resize", initWPGBModal);
window.addEventListener("load", initWPGBModal);
function initWPGBModal() {
    const windowWidth = window.innerWidth;
    if ( windowWidth <= 781 ) {
        MicroModal.init({
            onShow: function( modal, body, target ) {
            },
            openTrigger: 'data-wpgb-open',
            closeTrigger: 'data-wpgb-close',
            openClass: 'is-open'
        });
    }
}