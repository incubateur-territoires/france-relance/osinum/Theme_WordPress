const singleReadMore = document.querySelector( '.single-toggle' );
if ( singleReadMore ) {
    singleReadMore.addEventListener( 'click', function( e ) {
        e.preventDefault();
        const content = document.getElementById( e.target.getAttribute( 'aria-controls' ) );
        if ( content ) {
            content.classList.remove( 'single__read-more--hidden' );
            console.log(e)
            e.target.style.display = 'none';
        }
    } );
}