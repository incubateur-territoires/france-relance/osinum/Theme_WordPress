const enableVariationAttributeOnBlocks = [
    'core/button'
];

const setButtonVariationAttribute = ( settings, name ) => {
    // Do nothing if it's another block than our defined ones.
    if ( ! enableVariationAttributeOnBlocks.includes( name ) ) {
        return settings;
    }

    return Object.assign( {}, settings, {
        attributes: Object.assign( {}, settings.attributes, {
            buttonType: { type: 'string' }
        } ),
    } );
};
wp.hooks.addFilter(
    'blocks.registerBlockType',
    'osinum-diag/set-button-variation-attribute',
    setButtonVariationAttribute
);

wp.domReady( function() {
    wp.blocks.unregisterBlockStyle( 'core/button', 'fill' );
    wp.blocks.unregisterBlockStyle( 'core/button', 'outline' );

    wp.blocks.registerBlockStyle( 'core/button', {
        name: 'button-white',
        label: 'Blanc',
    } );
    wp.blocks.registerBlockStyle( 'core/button', {
        name: 'button-black',
        label: 'Noir',
    } );
    wp.blocks.registerBlockStyle( 'core/button', {
        name: 'button-blue-1-a',
        label: 'Bleu 1 A',
    } );
    wp.blocks.registerBlockStyle( 'core/button', {
        name: 'button-blue-2-a',
        label: 'Bleu 2 A',
    } );
    wp.blocks.registerBlockStyle( 'core/button', {
        name: 'button-green-1-a',
        label: 'Vert 1 A',
    } );
    wp.blocks.registerBlockStyle( 'core/button', {
        name: 'button-green-2-a',
        label: 'Vert 2 A',
    } );
    wp.blocks.registerBlockStyle( 'core/button', {
        name: 'button-yellow-1-a',
        label: 'Jaune 1 A',
    } );
    wp.blocks.registerBlockStyle( 'core/button', {
        name: 'button-orange-1-a',
        label: 'Orange 1 A',
    } );
    wp.blocks.registerBlockStyle( 'core/button', {
        name: 'button-orange-2-a',
        label: 'Orange 2 A',
    } );
    wp.blocks.registerBlockStyle( 'core/button', {
        name: 'button-red-1-a',
        label: 'Rouge 1 A',
    } );
    wp.blocks.registerBlockStyle( 'core/button', {
        name: 'button-pink-1-a',
        label: 'Rose 1 A',
    } );
    wp.blocks.registerBlockStyle( 'core/button', {
        name: 'button-purple-1-a',
        label: 'Violet 1 A',
    } );
} );