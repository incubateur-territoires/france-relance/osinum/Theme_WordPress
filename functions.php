<?php
/**
 * Functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package osinum-diag
 * @since 1.0.0
 */

/**
 * The theme version.
 *
 * @since 1.0.0
 */
define( 'OSINUM_DIAG_VERSION', wp_get_theme()->get( 'Version' ) );

/**
 * Add theme support.
 */
function osinum_diag_setup() {

	load_theme_textdomain( 'osinum-diag', get_template_directory() . '/languages' );

	register_nav_menus( array( 'primary-menu' => __( 'Primary', 'osinum-diag' ) ) );

	add_theme_support(
		'custom-logo',
		array(
			'height'               => 16,
			'width'                => 120,
			'flex-width'           => true,
			'flex-height'          => true,
			'unlink-homepage-logo' => true,
		)
	);
	add_theme_support( 'title-tag' );
	add_theme_support(
		'html5',
		array(
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'style',
			'script',
			'navigation-widgets',
		)
	);
	add_theme_support( 'responsive-embeds' );
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'block-template-parts' );
	add_theme_support( 'wp-block-styles' );
	add_theme_support( 'editor-styles' );
	add_editor_style( [
		'assets/css/style.css',
		'assets/css/editor.css'
	] );

	add_image_size( 'entry-thumbnail', 320, 150, true );
	add_image_size( 'square', 160, 160, false );
}
add_action( 'after_setup_theme', 'osinum_diag_setup' );

/**
 * Enqueue the CSS files.
 *
 * @since 1.0.0
 *
 * @return void
 */
function osinum_diag_styles() {
	$ext = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
	wp_register_style( 'swiper-css', get_theme_file_uri( 'assets/swiper/swiper.min.css' ) );
	wp_enqueue_style(
		'osinum-diag-style',
		get_stylesheet_uri(),
		[],
		OSINUM_DIAG_VERSION
	);
	wp_enqueue_style(
		'osinum-diag-shared-styles',
		get_theme_file_uri( 'assets/css/style'. $ext .'.css' ),
		[ 'swiper-css' ],
		OSINUM_DIAG_VERSION
	);
	wp_register_script( 'swiper-js', get_theme_file_uri( 'assets/swiper/swiper.min.js' ), [], false, true );
	wp_register_script( 'micromodal-js', get_theme_file_uri( 'assets/micromodal/micromodal.min.js' ), [], false, true );
	wp_register_script( 'sticky-js', get_theme_file_uri( 'assets/js/sticky.min.js' ), [], false, true );
	wp_enqueue_script(
		'osinum-diag-scripts',
		get_theme_file_uri( 'assets/js/scripts'. $ext .'.js' ),
		[ 'swiper-js', 'micromodal-js', 'sticky-js' ],
		OSINUM_DIAG_VERSION,
		true
	);
}
add_action( 'wp_enqueue_scripts', 'osinum_diag_styles' );

/**
 * Enqueue block editor script.
 *
 * @since 0.1
 *
 * @return void
 */
function osinum_diag_blocks_block_editor_script() {
	wp_enqueue_script( 'osinum_diag-editor-script', get_theme_file_uri( '/assets/editor/osinum_diag-editor-script.js' ), [ 'wp-blocks', 'wp-dom', 'wp-i18n' ], OSINUM_DIAG_VERSION, true );
}
add_action( 'enqueue_block_editor_assets', 'osinum_diag_blocks_block_editor_script' );

require_once get_theme_file_path( 'inc/diagnostic-back.php' );

require_once get_theme_file_path( 'inc/register-cpts.php' );

add_action( 'init', 'osinum_diag_acf_init', 0 );
function osinum_diag_acf_init() {
	if ( class_exists( 'acf' ) ) {
		require_once get_theme_file_path( 'inc/acf.php' );
		if ( function_exists( 'acf_register_block_type' ) ) {
			acf_register_block_type( [
				'name'              => 'posts-carousel',
				'title'             => __( 'Posts carousel', 'osinum-diag' ),
				'render_template'   => 'blocks/posts-carousel/posts-carousel.php',
				'category'          => 'theme',
				'icon'				=> 'slides'
			] );
			acf_register_block_type( [
				'name'              => 'images-carousel',
				'title'             => __( 'Images carousel', 'osinum-diag' ),
				'render_template'   => 'blocks/images-carousel/images-carousel.php',
				'category'          => 'theme',
				'icon'				=> 'slides'
			] );
		}
	}
}

// Template tags
require_once get_theme_file_path( 'inc/template-tags.php' );

// Filter core.
require_once get_theme_file_path( 'inc/filters.php' );

// Block style examples.
require_once get_theme_file_path( 'inc/register-block-styles.php' );

// Block pattern helper for the privacy policy.
require_once get_theme_file_path( 'inc/block-pattern-helper.php' );
