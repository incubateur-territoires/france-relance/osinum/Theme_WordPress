# Theme_WordPress

Thème WordPress développé pour osinumterritoires.fr

Tous les éléments de la charte graphique (icones, palettes, images etc...) réalisée par Praticable (https://praticable.fr/) sont librement accessibles et téléchargeables à l'adresse suivante :

https://www.figma.com/community/file/1215710352343118860/Osinum-TERRITOIRES---Maquettes