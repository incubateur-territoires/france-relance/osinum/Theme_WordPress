<?php
/**
 * Filter Diagnostic Back plugin.
 *
 * @package osinum-diag
 * @since 1.0.0
 */

 /**
  * Add theme styles to single PDF
  */
//add_action( 'ositer/pdf/single/head', 'osinum_diag_pdf_add_theme_style' );
function osinum_diag_pdf_add_theme_style() {
    $ext = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
    return sprintf(
        '<link rel="stylesheet" href="%1$s" media="all" />',
        get_theme_file_uri( 'assets/css/style'. $ext .'.css' )
    );
}