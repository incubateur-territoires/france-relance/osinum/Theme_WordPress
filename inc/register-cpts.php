<?php
/**
 * Register Custom Post Types and Taxonomies
 *
 * @package osinum-diag
 * @since 1.0.0
 */

add_action( 'init', 'osinum_diag_register_cpts' );
function osinum_diag_register_cpts() {
    $labels = array(
		'name'                  => _x( 'Team', 'Post Type General Name', 'osinum-diag' ),
		'singular_name'         => _x( 'Member', 'Post Type Singular Name', 'osinum-diag' ),
		'menu_name'             => __( 'Team', 'osinum-diag' ),
		'name_admin_bar'        => __( 'Team', 'osinum-diag' )
	);
	$args = array(
		'label'                 => __( 'Member', 'osinum-diag' ),
        'description'           => __( 'Lorem ipsum', 'osinum-diag' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-admin-users',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
        'show_in_rest'          => true
	);
	register_post_type( 'team', $args );
}

// Register Custom Taxonomy
function osinum_diag_register_taxs() {
	$labels = array(
		'name'                       => _x( 'Types', 'Taxonomy General Name', 'osinum-diag' ),
		'singular_name'              => _x( 'Type', 'Taxonomy Singular Name', 'osinum-diag' ),
		'menu_name'                  => __( 'Types', 'osinum-diag' )
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => false,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => false,
		'show_tagcloud'              => false,
		'rewrite'                    => false,
		'show_in_rest'               => true,
	);
	register_taxonomy( 'team_type', array( 'team' ), $args );

	$labels = array(
		'name'                       => _x( 'Formats', 'Taxonomy General Name', 'osinum-diag' ),
		'singular_name'              => _x( 'Format', 'Taxonomy Singular Name', 'osinum-diag' ),
		'menu_name'                  => __( 'Formats', 'osinum-diag' )
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => false,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => false,
		'show_tagcloud'              => false,
		'rewrite'                    => false,
		'show_in_rest'               => true,
	);
	register_taxonomy( 'format', array( 'resource' ), $args );
}
add_action( 'init', 'osinum_diag_register_taxs', 0 );