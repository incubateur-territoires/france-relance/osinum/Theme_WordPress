<?php
/**
 * ACF related functions
 *
 * @package osinum-diag
 */

/**
 * Hook on update field group to save custom group to local json
 */
add_action( 'acf/update_field_group', 'osinum_diag_update_field_group', 1, 1 );
function osinum_diag_update_field_group( $group ) {
    $local_field_groups_keys = [
        'group_6369090cc5c35',
        'group_636a61b6c0f9d',
        'group_636914e70483c',
        'group_637cd37453324',
        'group_637f7cfe3b1e0'
    ];
    if ( isset( $group[ 'key' ] ) && ! empty( $group[ 'key' ] ) ) {
        if ( in_array( $group[ 'key' ], $local_field_groups_keys ) ) {
            add_filter( 'acf/settings/save_json', 'osinum_diag_save_json', 9999, 1 );
        }
    }
    return $group;
}

function osinum_diag_save_json( $path ) {
    $path = get_theme_file_path() . '/json';
    return $path;
}

add_filter( 'acf/settings/load_json', 'osinum_diag_json_load_point' );
function osinum_diag_json_load_point( $paths ) {   
    // append path
    $paths[] = get_theme_file_path() . '/json';
    // return
    return $paths;
}