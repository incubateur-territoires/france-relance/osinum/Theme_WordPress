<?php
/**
 * Custom template tags for this theme
 *
 * @package osinum-diag
 */

 /**
  * Prints post published date
  *
  * @package osinum-diag
  */
function osinum_posted_on() {
    $time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';

    $custom_date = get_post_meta( get_the_ID(), 'post_date', true );
    if ( $custom_date ) {
        $formated_date = date_i18n( get_option( 'date_format' ), strtotime( $custom_date ) );
        $time_string = sprintf(
            $time_string,
            esc_attr( date_i18n( 'Y-m-d\TH:i:sP', strtotime( $custom_date ) ) ),
            esc_html( date_i18n( get_option( 'date_format' ), strtotime( $custom_date ) ) )
        );
    } else {
        $time_string = sprintf(
            $time_string,
            esc_attr( get_the_date( DATE_W3C ) ),
            esc_html( get_the_date() )
        );
    }
    
    echo '<span class="posted-on">';
    printf(
        /* translators: %s: Publish date. */
        '%s',
        $time_string // phpcs:ignore WordPress.Security.EscapeOutput
    );
    echo '</span>';
}

/**
 * Prints HTML with meta information about post author.
 *
 * @package osinum-diag
 */
function osinum_posted_by() {
    $custom_desc = get_post_meta( get_the_ID(), 'post_subtitle', true );
    if ( $custom_desc ) {
        echo '<span class="byline">';
        echo wpautop( $custom_desc );
        echo '</span>';
    } else {
        if ( ! get_the_author_meta( 'description' ) && post_type_supports( get_post_type(), 'author' ) ) {
            echo '<span class="byline">';
            printf(
                /* translators: %s: Author name. */
                esc_html__( 'By %s', 'osinum-diag' ),
                esc_html( get_the_author() )
            );
            echo '</span>';
        }
    }
}

/**
 * Prints HTML post meta about member cpt
 */
function osinum_diag_member_social() {
    $linkedin = get_post_meta( get_the_ID(), 'member_linkedin', true );
    $facebook = get_post_meta( get_the_ID(), 'member_facebook', true );
    $twitter = get_post_meta( get_the_ID(), 'member_twitter', true );
    $email = get_post_meta( get_the_ID(), 'member_email', true );
    $phone = get_post_meta( get_the_ID(), 'member_phone', true );
    ?>
    <div class="post-social">
        <?php if ( $linkedin || $facebook || $twitter ) : ?>
            <ul class="post-networks">
                <?php if ( $linkedin ) :
                printf(
                    '<li><a href="%s"><i class="icon__ icon-linkedin" aria-hidden="true"></i><span class="sr-only">%s</span></a></li>',
                    esc_url( $linkedin ),
                    __( 'View linkedin profile', 'osinum-diag' )
                );
                endif; ?>
                <?php if ( $facebook ) :
                printf(
                    '<li><a href="%s"><i class="icon__ icon-facebook" aria-hidden="true"></i><span class="sr-only">%s</span></a></li>',
                    esc_url( $facebook ),
                    __( 'View facebook profile', 'osinum-diag' )
                );
                endif; ?>
                <?php if ( $twitter ) :
                printf(
                    '<li><a href="%s"><i class="icon__ icon-twitter" aria-hidden="true"></i><span class="sr-only">%s</span></a></li>',
                    esc_url( $twitter ),
                    __( 'View twitter profile', 'osinum-diag' )
                );
                endif; ?>
            </ul>
        <?php endif; ?>
        <?php if ( $email ) : ?>
            <p><?php echo $email; ?></p>
        <?php endif; ?>
        <?php if ( $phone ) : ?>
            <p><?php echo $phone; ?></p>
        <?php endif; ?>
    </div>
    <?php
}

/**
 * Display post format icon
 */
function osinum_post_format_icon( $post_id ) {
    $formats = get_the_terms( $post_id, 'format' );
    if ( $formats ) {
        $format = $formats[0]; // take first format
        $icon = get_term_meta( $format->term_id, 'format_icon', true );
        if ( $icon ) {
            return '<span class="icon-format">' . wp_get_attachment_image( $icon, 'square', false, [ 'aria-hidden' => 'true' ] ) . '</span>';
        } else {
            return '<i class="icon__ icon-document-layout"></i>';
        }
    }
    return '<i class="icon__ icon-document-layout"></i>';
}