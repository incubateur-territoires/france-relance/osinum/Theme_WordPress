<?php
/**
 * Filter core.
 *
 * @package osinum-diag
 * @since 1.0.0
 */

function osinum_diag_excerpt_length( $length ) {
    return 22;
}
add_filter( 'excerpt_length', 'osinum_diag_excerpt_length', 999 );

function osinum_diag_excerpt_more( $more ) {
    return '...';
}
add_filter('excerpt_more', 'osinum_diag_excerpt_more');

add_filter( 'get_the_archive_title', 'osinum_diag_archive_title' );
function osinum_diag_archive_title($title) {
    if (is_category()) {
        $title = single_cat_title('', false);
    } elseif (is_tag()) {
        $title = single_tag_title('', false);
    } elseif (is_author()) {
        $title = '<span class="vcard">' . get_the_author() . '</span>';
    } elseif (is_tax()) { //for custom post types
        $title = sprintf(__('%1$s'), single_term_title('', false));
    } elseif (is_post_type_archive()) {
        $title = post_type_archive_title('', false);
    }
    return $title;
}

add_action( 'template_redirect', 'osinum_diag_template_redirect' );
function osinum_diag_template_redirect() {
    // Disable single post link for team CPT
    if ( is_singular( 'team' ) ) {
        wp_redirect( home_url(), 301 );
        exit;
    }
}

add_filter( 'post_thumbnail_html', 'osinum_default_thumbnail', 10, 5 );
function osinum_default_thumbnail( $html, $post_id, $post_thumbnail_id, $size, $attr ) {
    if ( empty( $html ) && 'resource' === get_post_type( $post_id ) || empty( $html ) && 'post' === get_post_type( $post_id ) ) {
        $html = sprintf(
            '<span class="thumbnail-placeholder"><span>%1$s</span><span>%2$s</span></span>',
            '<svg width="22" height="18" viewBox="0 0 22 18" fill="none" xmlns="http://www.w3.org/2000/svg" aria-hidden="true">
            <path d="M1.49995 7.56007L10.4999 12.7601C10.652 12.8478 10.8244 12.894 10.9999 12.894C11.1755 12.894 11.3479 12.8478 11.4999 12.7601L20.5 7.56007C20.6515 7.47216 20.7773 7.34599 20.8647 7.19418C20.9522 7.04237 20.9982 6.87026 20.9982 6.69507C20.9982 6.51988 20.9522 6.34776 20.8647 6.19596C20.7773 6.04415 20.6515 5.91797 20.5 5.83007L11.4999 0.630069C11.3479 0.5423 11.1755 0.496094 10.9999 0.496094C10.8244 0.496094 10.652 0.5423 10.4999 0.630069L1.49995 5.83007C1.34841 5.91797 1.22262 6.04415 1.13518 6.19596C1.04775 6.34776 1.00172 6.51988 1.00172 6.69507C1.00172 6.87026 1.04775 7.04237 1.13518 7.19418C1.22262 7.34599 1.34841 7.47216 1.49995 7.56007V7.56007ZM10.9999 2.65007L18 6.65007L10.9999 10.7001L3.99995 6.69007L10.9999 2.65007ZM19.5 10.4401L10.9999 15.3501L2.49995 10.4401C2.38641 10.3735 2.26086 10.3301 2.13049 10.3122C2.00012 10.2942 1.86749 10.3022 1.74022 10.3357C1.61295 10.3691 1.49352 10.4274 1.3888 10.5071C1.28408 10.5867 1.19611 10.6863 1.12995 10.8001C1.06383 10.9142 1.02094 11.0403 1.00376 11.1711C0.986578 11.3018 0.995437 11.4347 1.02983 11.5621C1.06422 11.6894 1.12346 11.8087 1.20415 11.913C1.28483 12.0174 1.38536 12.1047 1.49995 12.1701L10.4999 17.3701C10.652 17.4578 10.8244 17.504 10.9999 17.504C11.1755 17.504 11.3479 17.4578 11.4999 17.3701L20.5 12.1701C20.6145 12.1047 20.7151 12.0174 20.7958 11.913C20.8764 11.8087 20.9357 11.6894 20.9701 11.5621C21.0045 11.4347 21.0133 11.3018 20.9961 11.1711C20.979 11.0403 20.9361 10.9142 20.87 10.8001C20.8038 10.6863 20.7158 10.5867 20.6111 10.5071C20.5064 10.4274 20.387 10.3691 20.2597 10.3357C20.1324 10.3022 19.9998 10.2942 19.8694 10.3122C19.739 10.3301 19.6135 10.3735 19.5 10.4401V10.4401Z" fill="black"/>
            </svg>',
            get_post_type_object( get_post_type( $post_id ) )->labels->singular_name
        );
    }
    return $html;
}