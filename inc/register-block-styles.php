<?php
/**
 * Block styles.
 *
 * @package osinum-diag
 * @since 1.0.0
 */

/**
 * Register block styles
 *
 * @since 1.0.0
 *
 * @return void
 */
function osinum_diag_register_block_styles() {
}
add_action( 'init', 'osinum_diag_register_block_styles' );
