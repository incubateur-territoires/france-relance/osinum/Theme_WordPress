<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package osinum-diag
 */

get_header();
$description = get_the_archive_description();
?>

	<main id="primary" class="site-main">

        <?php if ( have_posts() ) : ?>

            <header class="entry-header">
                <?php if ( function_exists( 'seopress_display_breadcrumbs' ) ) : ?>
                    <div class="breadcrumbs">
                        <?php seopress_display_breadcrumbs(); ?>		
                    </div>
                <?php endif; ?>
                <?php the_archive_title( '<h1 class="entry-title">', '</h1>' ); ?>
                <?php if ( $description ) : ?>
                    <div class="archive-description"><?php echo wp_kses_post( wpautop( $description ) ); ?></div>
                <?php endif; ?>
            </header><!-- .page-header -->
            
            <div class="archive-content">
                <?php while ( have_posts() ) :
                    the_post();
                    get_template_part( 'template-parts/content', get_post_type() );
                endwhile; ?>
            </div>

            <?php the_posts_navigation(); ?>

        <?php else : ?>

            <?php get_template_part( 'template-parts/content/content-none' ); ?>

        <?php endif; ?>

	</main><!-- #main -->

<?php
get_footer();