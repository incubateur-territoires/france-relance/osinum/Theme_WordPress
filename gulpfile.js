const gulp = require("gulp"),
    sass = require("gulp-sass")(require("sass-embedded")),
    sassGlob = require("gulp-sass-glob"),
    postcss = require("gulp-postcss"),
    autoprefixer = require("autoprefixer"),
    concat = require("gulp-concat"),
    rename = require("gulp-rename"),
    uglify = require("gulp-uglify"),
    cleanCSS = require("gulp-clean-css");

// js file paths
const componentsJsPath = "assets/js/components/*.js", // component js files
    scriptsJsPath = "assets/js"; //folder for final scripts.js/scripts.min.js files

// css file paths
const cssFolder = "assets/css", // folder for final style.css/style-fallback.css files
    scssFilesPath = "assets/sass/**/*.scss"; // scss files to watch

/* Gulp watch tasks */
// This task is used to convert the scss to css and compress it.
gulp.task("sass", function () {
    return gulp
        .src(scssFilesPath)
        .pipe(sassGlob({ sassModules: true }))
        .pipe(sass().on("error", sass.logError))
        .pipe(postcss([autoprefixer()]))
        .pipe(gulp.dest(cssFolder))
        .pipe(rename({ suffix: ".min" }))
        .pipe(cleanCSS())
        .pipe(gulp.dest(cssFolder));
});

// This task is used to combine all js files in a single scripts.min.js.
gulp.task("scripts", function () {
    return gulp
        .src([scriptsJsPath + "/util.js", componentsJsPath], {
            allowEmpty: true
        })
        .pipe(concat("scripts.js"))
        .pipe(gulp.dest(scriptsJsPath))
        .pipe(rename({ suffix: ".min" }))
        .pipe(uglify())
        .pipe(gulp.dest(scriptsJsPath));
});

gulp.task(
    "watch",
    gulp.series(["sass", "scripts"], function () {
        gulp.watch("assets/sass/**/*.scss", gulp.series(["sass"]));
        gulp.watch(componentsJsPath, gulp.series(["scripts"]));
    })
);