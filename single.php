<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package osinum-diag
 */

get_header();
?>

    <main id="primary" class="site-main">
        <?php
        /* Start the Loop */
        while ( have_posts() ) :
            the_post();

            get_template_part( 'template-parts/content-single' );

        endwhile; // End of the loop.
        ?>
    </main>

<?php
get_footer();
