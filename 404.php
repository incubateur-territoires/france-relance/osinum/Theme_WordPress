<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package osinum-diag
 */

get_header();
?>

	<main id="primary" class="site-main">
        <section class="no-results not-found entry-content">

            <header class="page-header">
                <h1 class="page-title"><?php esc_html_e( '404', 'osinum-idag' ); ?></h1>
            </header><!-- .page-header -->

            <p><?php printf( __( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Please <a href="%s">contact us</a>.', 'osinum-diag' ), esc_url( get_home_url() . '/contact' ) ); ?></p>
			<div class="wp-block-button">
				<a href="<?php echo esc_url( get_home_url() ); ?>" class="wp-block-button__link"><?php _e( 'Go to the homepage', 'osinum-diag' ); ?></a>
			</div>

        </section>

	</main><!-- #main -->

<?php
get_footer();