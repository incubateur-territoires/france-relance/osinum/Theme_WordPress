
<?php
/**
 * images Carousel Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */
$id = 'wp-block-images-carousel-' . $block[ 'id' ];
if ( ! empty( $block[ 'anchor' ] ) ) {
    $id = $block[ 'anchor' ];
}

$className = 'wp-block-images-carousel';
if ( ! empty( $block [ 'className' ] ) ) {
    $className .= ' ' . $block[ 'className' ];
}

$images = get_field( 'images' );
if ( empty( $images ) && ! $is_preview ) {
    return;
}
$modal = get_field( 'modal' );
$modal_id = 'modal-' . $id;
?>
<div id="<?php echo esc_attr( $id ); ?>" class="<?php echo esc_attr( $className ); ?>">
    <?php if ( $is_preview ) : ?>
        <?php if ( empty( $images ) ) : ?>
            <p><?php _e( 'Please select images', 'osinum-diag' ); ?></p>
        <?php else : ?>
            <div class="swiper-element">
                <div class="swiper" data-slides="1.3" data-slides-mobile="1.1" data-space="24">
                    <div class="swiper-wrapper">
                        <?php foreach( $images as $image ) : ?>
                            <div class="swiper-slide">
                                <?php wp_get_attachment_image( $image, 'medium-large' ); ?>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    <?php else : ?>
        <div class="swiper-element">
            <div class="swiper" data-slides="1.3" data-slides-mobile="1.1" data-space="24">
                <div class="swiper-wrapper">
                    <?php $index = 0;
                    foreach( $images as $image ) : ?>
                        <?php if ( $modal ) : ?>
                            <div class="swiper-slide" data-slide-index="<?php esc_attr_e( $index ); ?>">
                                <a href="<?php echo esc_url( wp_get_attachment_image_url( $image, 'large' ) ); ?>" data-gallery-open="<?php esc_attr_e( $modal_id ); ?>">
                                    <?php echo wp_get_attachment_image( $image, 'medium-large' ); ?>
                                </a>
                            </div>
                        <?php else : ?>
                            <div class="swiper-slide">
                                <?php echo wp_get_attachment_image( $image, 'medium-large' ); ?>
                            </div>
                        <?php endif; ?>
                    <?php $index++; endforeach; ?>
                </div>
            </div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
        </div>
        <?php if ( $modal ) : ?>
            <div id="<?php esc_attr_e( $modal_id ); ?>" class="gallery-modal micromodal-slide" aria-hidden="true">
                <div tabindex="-1" class="modal__overlay" data-gallery-close>
                    <div class="modal__container">
                        <button class="modal__close" aria-label="<?php _e( 'Close', 'osinum-diag' ); ?>" data-gallery-close>
                            <svg aria-hidden="true" class="icon fermer"><use xlink:href="#fermer"></use></svg>
                        </button>
                        <div class="swiper-element">
                            <div class="swiper" data-slides="1" data-slides-mobile="1">
                                <div class="swiper-wrapper">
                                    <?php
                                    $index = 0;
                                    foreach( $images as $image ) :
                                    ?>
                                        <div class="swiper-slide" data-slide-index="<?php esc_attr_e( $index ); ?>">
                                            <?php echo wp_get_attachment_image( $image, 'medium-large' ); ?>
                                        </div>
                                    <?php $index++; endforeach; ?>
                                </div>
                            </div>
                            <div class="swiper-button-prev"></div>
                            <div class="swiper-button-next"></div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    <?php endif; ?>    
</div>