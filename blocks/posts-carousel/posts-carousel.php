<?php

/**
 * Posts Carousel Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */
$id = 'wp-block-posts-carousel-' . $block[ 'id' ];
if ( ! empty( $block[ 'anchor' ] ) ) {
    $id = $block[ 'anchor' ];
}

$className = 'wp-block-posts-carousel';
if ( ! empty( $block [ 'className' ] ) ) {
    $className .= ' ' . $block[ 'className' ];
}

if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

// Load values and handle defaults.
$post_type = get_field( 'post_type' ) ? : 'post';
$posts_per_page = get_field( 'posts_per_page' ) ? : 5;
$class = 'swiper-slide';

$args = [
    'post_type'         => $post_type,
    'post_status'       => 'publish',
    'posts_per_page' => $posts_per_page
];
if ( get_field( 'posts' ) ) {
    $args[ 'post__in' ] = get_field( 'posts' );
}
if ( get_field( 'popular' ) ) {
    $args[ 'post__in' ] = get_option( 'options_' . $post_type . '_populars' );
    $class .= ' post-card--large';
}
$carousel_posts = new WP_Query( $args );

if ( ! $carousel_posts ) {
    return;
}

$slides = get_field( 'slides' ) ? : 3.2;
$slides_mobile = get_field( 'slides_mobile' ) ? : 1.1;
$slides_gap = get_field( 'slides_gap' ) ? : 24;
?>
<div id="<?php echo esc_attr( $id ); ?>" class="<?php echo esc_attr( $className ); ?>">
    <div class="swiper-element">
        <div class="swiper" data-slides="<?php esc_attr_e( $slides ); ?>" data-slides-mobile="<?php esc_attr_e( $slides_mobile ); ?>" data-space="<?php esc_attr_e( $slides_gap ); ?>">
            <div class="swiper-wrapper">
            <?php
                while ( $carousel_posts->have_posts() ) :
                    $carousel_posts->the_post();
                    get_template_part( 'template-parts/content', get_post_type(), [ 'class' => $class, 'heading' => 'h3' ] );
                endwhile; // End of the loop.
                wp_reset_postdata();
                ?>
            </div>
        </div>
        <?php if ( ! $is_preview ) : ?>
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
        <?php endif; ?>
    </div>
</div>