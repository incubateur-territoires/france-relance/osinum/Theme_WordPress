<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package osinum-diag
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'osinum-diag' ); ?></a>

	<header id="masthead" class="site-header has-light-color has-primary-dark-background-color">
		<div class="site-branding">
			<?php if ( has_custom_logo() ) : 
				echo '<a href="'.get_home_url().'">'.wp_get_attachment_image( get_theme_mod( 'custom_logo' ), [ 220, 24 ] ).'</a>';
			else : ?>
				<p class="site-title"><?php echo get_bloginfo( 'name', 'display' ); ?></p>
			<?php endif; ?>
			<?php
			if ( ! is_front_page() ) :
				?>
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="site-home-link" rel="home"><?php _e( 'Home', 'osinum-diag' ); ?></a>
				<?php
			endif; ?>
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false" aria-label="<?php esc_html_e( 'Primary Menu', 'osinum-diag' ); ?>">
				<span></span>
				<span></span>
				<span></span>
			</button>
		</div><!-- .site-branding -->

		<nav id="site-navigation" class="main-navigation">
			<p class="main-navigation__title"><?php _e( 'Menu', 'osinum-diag' ); ?></p>
			<?php
			wp_nav_menu(
				array(
					'theme_location' => 'primary-menu',
					'menu_id'        => 'primary-menu',
				)
			);
			?>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->